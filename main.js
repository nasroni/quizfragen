// Load questions
var xmlhttp = new XMLHttpRequest();
xmlhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        questions = JSON.parse(this.responseText);
        if (window.location.pathname == "/read.html") {
            var workobj = $(".alert-boot");
            $(workobj).addClass("alert-info").removeClass("alert-warning")
            $(workobj).text("Bereit :) Bitte auswählen:");
            renderQuestions();
        }
    }
};
xmlhttp.open("GET", "/data/fragen.json", true);
xmlhttp.send();

// Vars START
var testament = "";
var book = "";
var chapter = "";
var questions = [];

var books_at = {
    "1. Mose": 50,
    "2. Mose": 40,
    "3. Mose": 27,
    "4. Mose": 36,
    "5. Mose": 34,
    "Josua": 24,
    "Richter": 21,
    "Ruth": 4,
    "1. Samuel": 31,
    "2. Samuel": 24,
    "1. Könige": 22,
    "2. Könige": 25,
    "Esra": 10,
    "Nehemia": 13,
    "Esther": 10,
    "Sprüche": 31,
    "Prediger": 12,
    "Daniel": 12,
    "Hosea": 14,
    "Joel": 4,
    "Amos": 9,
    "Obadja": 1,
    "Jona": 4,
    "Micha": 7,
    "Nahum": 3,
    "Habakuk": 3,
    "Zephanja": 3,
    "Haggai": 1,
    "Maleachi": 3
};
var books_nt = {
    "Matthäus": 28,
    "Markus": 16,
    "Lukas": 24,
    "Johannes": 21,
    "Apostelgeschichte": 28,
    "Römer": 16,
    "1. Korinther": 16,
    "2. Korinther": 13,
    "Epheser": 6,
    "Philipper": 4,
    "Offenbarung": 22
}

// Vars END

// Event handlers START
$("button").on("click", function (e) {
    link = $(e.target).find("a").attr("href");
    window.location = link;
});

$("#at-nt-selector").on("change", function (e) {
    testament = $(e.target).val();
    renderBooks();
    book = $("#book-selector").val();
    renderChapters();
});

$("#book-selector").on("change", function (e) {
    book = $(e.target).val();
    renderChapters();
});

$("#chapter-selector").on("change", function (e) {
    chapter = $(e.target).val();
    renderQuestions();
});

$(document).ready(function () {
    if (window.location.pathname == "/read.html") {
        // boot
        testament = $("#at-nt-selector").val();
        book = $("#book-selector").val();
        chapter = $("#chapter-selector").val();
        renderBooks();
        renderChapters();
    }
});
// Event handlers END

// functions START
function renderBooks() {
    books = testament == "at" ? Object.keys(books_at) : Object.keys(books_nt);
    books_html = "";
    books.forEach(book => {
        if ("" == books_html) {
            books_html = '<option value="' + book + '" selected>' + book + '</option>';
        } else {
            books_html += '<option value="' + book + '">' + book + '</option>';
        }
    });
    $("#book-selector").html(books_html);
}

function renderChapters() {
    chapters = testament == "at" ? books_at[book] : books_nt[book];
    chapters_html = "";
    for (var i = 1; i <= chapters; i++) {
        if ("" == chapters_html) {
            chapters_html = '<option value="' + i.toString() + '" selected> ' + i.toString() + '</option>';
        } else {
            chapters_html += '<option value="' + i.toString() + '">' + i.toString() + '</option>';
        }
    };
    $("#chapter-selector").html(chapters_html);
    renderQuestions();
}

function renderQuestions() {
    var question_set = filterQuestions();
    var questions_html = "";
    question_set.forEach(function (data) {
        var ans = data.antworten;
        var question = '<div class="question">';
        var text = data.frage.replace(/"/g, '&quot;') + "\n" + "a) " + ans.A + "\nb) " + ans.B + "\nc) " + ans.C + "\nd) " + ans.D;
        var cor = data.korrekt;
        var frage = bibleLink(data.frage);
        question += '<p class="qtext">' + frage + '</p>';
        var thisOne = cor == "a" ? ' cor' : '';
        question += '<p class="qa' + thisOne + '">a) ' + ans.A + '</p>';
        var thisOne = cor == "b" ? ' cor' : '';
        question += '<p class="qa' + thisOne + '">b) ' + ans.B + '</p>';
        var thisOne = cor == "c" ? ' cor' : '';
        question += '<p class="qa' + thisOne + '">c) ' + ans.C + '</p>';
        var thisOne = cor == "d" ? ' cor' : '';
        question += '<p class="qa' + thisOne + '">d) ' + ans.D + '</p>';
        if (ans.E) {
            var thisOne = cor == "e" ? ' cor' : '';
            question += '<p class="qa' + cor == "e" ? "cor" : "" + '">e) ' + ans.E + '</p>';
            text += "\ne) " + ans.E;
        }
        question += '<div class="btn-group w-100" role="group">';
        question += '<button type="button" class="btn btn-outline-success showAnswer">Zeige Antwort</button>';
        question += '<button type="button" class="btn btn-outline-info secondary openDeepl" data="' + text + '">Deutsch</button>';
        question += '</button></div>';
        question += '</div><hr>';
        questions_html += question;
    });

    $(".questions").html(questions_html);

    $(".showAnswer").on("click", function (e) {
        showAnswer(e);
    });
    $(".openDeepl").on("click", function (e) {
        deepl(e);
    });
    $(".showAnswer").on("touch", function (e) {
        showAnswer(e);
    });
    $(".openDeepl").on("touch", function (e) {
        deepl(e);
    });
}

function showAnswer(e) {
    $(e.target).parent().parent().find(".cor").addClass("green");
}

function deepl(e) {
    var text = encodeURI($(e.target).attr("data"));
    var url = "https://www.deepl.com/translator#en/de/";
    url += text;
    var win = window.open(url, '_blank');
    win.focus();
}

function filterQuestions() {
    var cur_questions = questions.filter(function (quest) {
        if (quest.buch != book) {
            return false;
        }
        if (quest.kapitel != chapter) {
            return false;
        }
        return true;
    });
    return cur_questions;
}

function bibleLink(data) {
    place = data.match(/\([1-5]?\.? ?[A-Za-z][a-zäöü]{1,20}.? ?[0-9]{1,2}[:;,.]?[0-9]*\)/g);
    if (place) {
        place = place[0].replace("(", "").replace(")", "");
        link = "https://r.bwk.one/" + place;
        a_tag = '<a href="' + link + '" target="_blank">' + place + "</a>";
        var res = data.replace(/\([1-5]?\.? ?[A-Za-z][a-zäöü]{1,20}.? ?[0-9]{1,2}[:;,.]?[0-9]*\)/g, "(" + a_tag + ")");
        return res;
    } else {
        return data;
    }
}